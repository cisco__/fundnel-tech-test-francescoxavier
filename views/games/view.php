<?php
$session = Yii::$app->session;          
                
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Games */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Games', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if($model->creatorId == $session['idUser'])            
                { echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); }
        ?>
        <?php /* Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */ ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'createDate',
            [
                'attribute'=>'Genre',
                'value'=>$genre,
            ],
            [
                'attribute'=>'photo',
                'value'=>'http://localhost'.Yii::$app->homeUrl.'images/'.$model->gameCover,
                'format' => ['image',['width'=>'100','height'=>'100']],
            ],
            'descr:ntext',
            'developer',
            'creatorId',
            'publisher',
            
        ],
    ]) ?>
<?php 
if($session['idUser'])
{
?>   
<textarea id='Isicomment' rows='4' cols='60' ></textarea>
<br /><br />
<button id='comment'>Comment Now</button>    
<br /><br />
<?php } ?>
<h1>Comments</h1>
<div>
<?php 
foreach ($comments as $key) {
?>
<div class="view">
    <p><?php echo $key->comments; ?></p>
</div>
<?php    
}
?>
</div>
<script type="text/javascript">
$(function() {
    var gameID = '<?php echo $model->id; ?>';
    var userID = '<?php echo Yii::$app->session['idUser']; ?>';
    var url = '<?php echo "http://".$_SERVER["SERVER_NAME"]. Yii::$app->homeUrl."games"; ?>';
    var comment = "";
    $("#comment").click( function()
    {
        
        comment = $("#Isicomment").val();
        var formData = {comments:comment,game:gameID,user:userID,_csrf : '<?=Yii::$app->request->getCsrfToken()?>'};
        $.ajax({
                    url : url,
                    type: "POST",
                    data : formData,
                    success: function(data, textStatus, jqXHR)
                    {
                        location.reload(true);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                 
                    }
                });
    });
});
</script>