<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\gamesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Games';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="games-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Games', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            'name',
            // 'createDate',
            // 'creatorId',
            'gameCover'=>array('format' => 'html','value' => function($data) 
                { 
                    return Html::a(Html::img('http://localhost'.Yii::$app->homeUrl.'images/'.$data['gameCover'],['width'=>'50']), ['games/view', 'id' => $data['id']], ['class' => 'profile-link']) ;
                }),
            // 'descr:ntext',
            // 'developer',
            // 'publisher',
            // 'genreID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
