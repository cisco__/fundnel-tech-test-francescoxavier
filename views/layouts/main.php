<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/css/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::$app->request->baseUrl; ?>/css/form.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <title><?= Html::encode($this->title) ?></title>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="logo"><?php echo Html::encode(Yii::$app->name); ?></div>
    </div><!-- header -->

    <div id="mainmenu">
       <?php

       if(isset($session['nameUser']) || $session['nameUser']!= '')
       {
            echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => 
                [
                    ['label' => 'Home', 'url' => ['/home']],
                    ['label' => 'Games', 'url' => ['/games']],
                    ['label' => 'Create Games Page', 'url' => ['/games/create']],
                    ['label' => 'Logout('.$session['nameUser'].')', 'url' => ['/users/logout']],
                ],
            ]);
       }else{
            echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => 
                [
                    ['label' => 'Home', 'url' => ['/home']],
                    ['label' => 'Games', 'url' => ['/games']],
                    // ['label' => 'Create Games Page', 'url' => ['/games/create']],
                    ['label' => 'Login', 'url' => ['/users/login']],
                ],
            ]);
       }
        
        
?>
        
    </div><!-- mainmenu -->

    
        <?= $content ?>
    
    <div class="clear"></div>

    <div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> by Francesco.<br/>
        All Rights Reserved.<br/>
        <?php echo Yii::powered(); ?>
    </div><!-- footer -->

</div><!-- page -->
<script type="text/javascript">
    $('#signupTombol').on('click', function(event){
    {
        window.open("create","_self")
    }
})
</script>
</body>
</html>
