<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;


class HomeController extends \yii\web\Controller
{
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
    	$model = new Users;
    	if(isset($_POST['Users']))
		{
			$request = Yii::$app->request;
			$data = Users::find()->where(['email' => $_POST['Users']['email'],'password' => $_POST['Users']['password']])->one();
			if(isset($data))
			{
				$session = Yii::$app->session;
				$session->set('idUser',$data->id);
				// $this->redirect(array('view','id'=>$model->id));
			}else{
				var_dump($data);
			}
			
			
		}
    	return $this->render('login', ['model' => $model,]);
    }
}
