<?php
namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;
use app\models\GameComments;



class UsersController extends \yii\web\Controller
{
    
    public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionLogin()
    {
    	$model = new Users;
    	if(isset($_POST['Users']))
		{
			$request = Yii::$app->request;
			$data = Users::find()->where(['email' => $_POST['Users']['email'],'password' => $_POST['Users']['password']])->one();
			if(isset($data))
			{
				$session = Yii::$app->session;			
				$session->set('idUser',$data->id);
				$session->set('nameUser',$data->name);
				
				Yii::$app->response->redirect(array('home'));
			}else{
				var_dump($data);
			}
			
			
		}
    	return $this->render('login', ['model' => $model,]);
    }

    public function actionLogout()
    {
    	$session = Yii::$app->session;
    	// close a session
		$session->close();
		$session = Yii::$app->session;
		$session->remove('idUser');
		$session->remove('nameUser');
		$session->destroy();
		
    	Yii::$app->response->redirect(array('users/login'));
    }

    public function actionGetfromajax()
    {
    	$session = Yii::$app->session;
    	$request = Yii::$app->request;
    	$comments = $request->post('comments'); 
    	$gameID = $request->post('gameID'); 
        $model= new GameComments;
        $model->gameID=$gameID;
        $model->comments=$comments;
        $model->userID=$session['idUser'];
        $model->createDate=date('Y-m-d:H:i:s');
        $model->save(false);
        echo json_encode("OK");
    }

    public function actionCreate()
    {

        
        
        $model = new Users();

        if ($model->load(Yii::$app->request->post()) && $model->save() ) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}

