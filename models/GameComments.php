<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gameComments".
 *
 * @property integer $id
 * @property integer $gameID
 * @property string $comments
 * @property integer $userID
 * @property string $createDate
 *
 * @property Game $game
 * @property Users $user
 */
class GameComments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gameComments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gameID', 'comments', 'userID', 'createDate'], 'required'],
            [['gameID', 'userID'], 'integer'],
            [['comments'], 'string'],
            [['createDate'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gameID' => 'Game ID',
            'comments' => 'Comments',
            'userID' => 'User ID',
            'createDate' => 'Create Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(Game::className(), ['id' => 'gameID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'userID']);
    }

    /**
     * @inheritdoc
     * @return GamesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new GamesQuery(get_called_class());
    }
}
