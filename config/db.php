<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=gamesdb',
    'username' => 'root',
    'password' => 'secret',
    'charset' => 'utf8',
];
